"""
Tests for sum function
"""

import unittest
from myfunctions import MySum

class SumTestCase(unittest.TestCase):
    def test_add_two_numbers(self):
        res = MySum(3, 4)
        self.assertEqual(res, 7)
